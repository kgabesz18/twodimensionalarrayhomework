package kg.twodimensonal;

public class ArrayCreator {
	
	public static String[][] loadArray(String[][] array) {
		
		for(int i = 0; i < array.length; i++) {
			for(int j = 0; j < array[i].length; j++) {
				array[i][j] = "X";
			}
		}
		
		return array;
	}
	
	public static void printArray(String[][] array) {
		
		for(int i = 0; i < array.length; i++) {
			for(int j = 0; j < array[i].length; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
		
	}
	
	public static String[][] drawDiagonalLeftToRight(String[][] array) {
		
		for(int i = 0; i < array.length; i++) {
			for(int j = 0; j < array[i]. length; j++) {
				if(i == j) {
					array[i][j] = "O";
				}
			}
		}
		return array;
	}
	
	public static String[][] drawDiagonalRightToLeft(String[][] array) {
		for(int i = 0; i < array.length; i++) {
			for(int j = 0; j < array[i].length; j++) {
				if(i + j == array.length - 1) {
					array[i][j] = "O";
				}
			}
		}
		return array;
	}
	
	public static String[][] drawVerticalLine(String[][] array) {
		for(int i = 0; i < array.length; i++) {
			for(int j = 0; j < array[i].length; j++) {
				if(j == array.length / 2) {
					array[i][j] = "O";
				}
			}
		}
		return array;
	}
	
	public static String[][] drawHorizontalLine(String[][] array) {
		for(int i = 0; i < array.length; i++) {
			for(int j = 0; j < array[i].length; j++) {
				if(i == array.length / 2) {
					array[i][j] = "O";
				}
			}
		}
		return array;
	}
	
	public static void createArrayContent(int row, int column) {
		
		int numberOfRows = row;
		int numberOfColumns = column;
		
		String[][] emptyArray = new String[numberOfRows][numberOfColumns];
		
		String[][] originalArray = loadArray(emptyArray);
		
		System.out.println("Original array: ");
		System.out.println();
		printArray(originalArray);
		
		System.out.println();
		
		System.out.println("Array after modification: ");
		System.out.println();
		String[][] modifiedArray = drawDiagonalLeftToRight(originalArray);
		drawDiagonalRightToLeft(modifiedArray);
		drawVerticalLine(modifiedArray);
		drawHorizontalLine(modifiedArray);
		
		printArray(modifiedArray);
		
		
		
		
		
		
		
	}

}
